/**
 * Created by Fresh on 25/6/2558.
 */
/**
 * Created by Fresh on 24/6/2558.
 */
var app= angular.module('sbAdminApp')
    .controller('inputCtrl', function ($scope,$filter,$interval,ngTableParams,GridService,ENDPOINTHEAD,ENDPOINTTAIL_INPUT) {

       // var data = NameService.data;

        $scope.endpoint = 'http://' + gENDPOINTHEAD + gENDPOINTTAIL_INPUT;

        $scope.content = "xxxx";
        $scope.tableInputParams = new ngTableParams({
            page: 1,            // show first page
            count: 100           // count per page
        }, {
            groupBy: 'nodeid',
            //total: data.length, // length of data
            getData: function ($defer, params) {
                GridService.getData($defer,params,params.filter(), $scope.endpoint,true);
            }
        });
        if (gintervalSetionInput) {
            gintervalSetionInput = $interval(function () {
                                        $scope.tableInputParams.reload();
                                    }, 5000);
        }
        $scope.update = function(){

            alert();
        }
    });

app.service("GridService", function($http, $filter){

    function filterData(data, filter){
        return $filter('filter')(data, filter)
    }

    function orderData(data, params){
        return params.sorting() ? $filter('orderBy')(data, params.orderBy()) : filteredData;
    }

    function sliceData(data, params){
        return data.slice((params.page() - 1) * params.count(), params.page() * params.count())
    }

    function transformData(data,filter,params){
        return sliceData( orderData( filterData(data,filter), params ), params);
    }

    var service = {
        cachedData:[],
        $defer:null,
        params:null,
        filter:null,
        getData:function($defer, params, filter,endpoint,loadInCachedData){

            if(service.cachedData.length>0){

                if(loadInCachedData)
                {
                    $http.get(endpoint).success(function(resp)
                    {
                       for(i=0;i<resp.length;i++)
                       {
                           device = resp[i];

                           var selectCache ;
                           angular.forEach(service.cachedData, function (data) {
                               if(device.id==data.id)
                               {
                                   selectCache = data;
                                   return;
                               }
                           });
                           if(selectCache)
                           {
                               var difference = Math.floor(Date.now()/ 1000) - Math.floor(device.time);
                               var minutesDifference  = Math.floor(difference/60);
                               var timeShow = "";
                               if(minutesDifference>60)
                               {
                                   timeShow = "inactive";
                                   selectCache.showTime = timeShow;
                               }
                               else
                               {
                                   var secondsDifference = Math.floor(difference);
                                   if(minutesDifference<1)
                                   {
                                       timeShow = secondsDifference+"s";
                                   }else{
                                       var minutes =   Math.floor(secondsDifference/60);
                                       var seconds = secondsDifference%60;
                                       timeShow = minutes+"m "+seconds+"s";
                                   }
                                   selectCache.showTime = timeShow;
                               }

                               selectCache.value = device.value;

                           }else{
                               service.cachedData.push(device);
                           }
                       }
                        var transformedData = transformData(service.cachedData,filter,params)
                        $defer.resolve(transformedData);

                    });

                } else {

                    var filteredData = filterData(service.cachedData, filter);
                    var transformedData = sliceData(orderData(filteredData, params), params);
                    params.total(filteredData.length)
                    $defer.resolve(transformedData);

                }
            }
            else{

                $http.get(endpoint).success(function(resp)
                {
                    angular.copy(resp,service.cachedData)
                    params.total(resp.length)

                    var transformedData = transformData(resp,filter,params)
                    for(i=0;i<resp.length;i++) {
                        device = resp[i];
                        device.showTime = "Watting...";
                    }
                    $defer.resolve(transformedData);
                });
            }
        }
    };
    return service;
});