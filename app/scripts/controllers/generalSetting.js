﻿'use strict';
var app = angular.module('sbAdminApp');

app.controller('generalSettingCtrl', function ($scope, ENDPOINTHEAD, ENDPOINTTAIL_INPUT) {
    $scope.ENDPOINTHEAD = gENDPOINTHEAD;
   
    $scope.ENDPOINTTAIL_INPUT = gENDPOINTTAIL_INPUT;
    $scope.clearSetting = function () {
        $scope.ENDPOINTHEAD = gENDPOINTHEAD;
        $scope.ENDPOINTTAIL_INPUT = gENDPOINTTAIL_INPUT;
    }
    $scope.SaveSetting = function () {
       gENDPOINTHEAD = $scope.ENDPOINTHEAD;
       gENDPOINTTAIL_INPUT = $scope.ENDPOINTTAIL_INPUT;
       }
   });