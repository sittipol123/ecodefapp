/**
 * Created by Fresh on 24/6/2558.
 */
angular.module('sbAdminApp')
    .controller('ButtonsCtrl', function ($scope) {
        $scope.singleModel = 1;

        $scope.radioModel = 'Middle';

        $scope.checkModel = {
            left: false,
            middle: true,
            right: false
        };
    });