/**
 * Created by Fresh on 24/6/2558.
 */



/**
 * Created by Fresh on 24/6/2558.
 */
angular.module('sbAdminApp')
    .controller('AlertDemoCtrl', function ($scope) {
        $scope.alerts = [
            { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
            { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
        ];

        $scope.addAlert = function() {
            $scope.alerts.push({msg: 'Another alert!'});
        };
        $scope.addMsg = function() {
            $scope.alerts.push({type: 'success', msg: 'Another Message'});
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
    });