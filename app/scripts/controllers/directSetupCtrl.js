/**
 * Created by Fresh on 29/6/2558.
 */
angular.module('sbAdminApp')
    .config(function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
    })
    .controller('directSetupCtrl', function ($scope, $position, $http) {

        $scope.apWifi= 'Home';
        $scope.xmlhttp;// = new XMLHttpRequest();
        $scope.WifiResult = "-"; 
        $scope.deviceInfo = {
            IP: '192.168.4.1',
            templateUrl: 'myPopoverTemplate.html',
            title: 'Title',
            APName :'true_homewifi_4CA',
            APpassword: '00000AZH',
            password: '',
            callServer: false,
            plugName:"",
            plugPassword:"",
            led13: true,
            power1: false,
            power2: false,
            power3: false,
            pwoer4: false
        };
        $scope.CallServerClick=function()
        {
            $scope.deviceInfo.callServer = !$scope.deviceInfo.callServer;
        }
        $scope.status = {
            IPConnected: "N", //N=new,D=doing,E=End,F=fail
            WifiAPconnected: "N",
            SetDevice: "N",
            GetInfo: "N",
            SetServer: "N",
            SendLed13: "N",
            SetPlugName: "N",
            SendPower: "N",
            SendRefreshStatus:"N"
        };
        $scope.SendLed13 = function () {
            switch ($scope.status.SendLed13) {
                case "N":
                    led13 = ($scope.deviceInfo.led13) ? "1" : "0";
                    //http://192.168.4.1/O=www.google.com,80,T,
                    endpoint = "http://" + $scope.deviceInfo.IP + "/@G" + led13;
                    
                    $scope.status.SendLed13 = "D";
                    $scope.xmlhttp = new XMLHttpRequest();
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            //$scope.ResultChecking = xmlhttp.responseText;
                            if ($scope.xmlhttp.status == 200) {
                                $scope.ResultRefresh = $scope.xmlhttp.responseText;                               
                                $scope.status.SendLed13 = "E";
                            } else {
                                $scope.status.SendLed13 = "F";
                            }
                            $scope.$apply();
                        }
                    }
                    $scope.xmlhttp.open("GET", endpoint, true);
                    $scope.xmlhttp.send();
                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.SendLed13 = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.SendLed13 = "N";
                    break;
                case "F":
                    $scope.status.SendLed13 = "N";
                    break;
                default:
                    $scope.status.SendLed13 = "N";
                    break;
            }
        }
        
        $scope.SetPlugName = function () {

            switch ($scope.status.SetPlugName) {
                case "N":
                    //http://192.168.4.1/O=www.google.com,80,T,
                    endpoint = "http://" + $scope.deviceInfo.IP + "/B=" + $scope.deviceInfo.plugName + "," + $scope.deviceInfo.plugPassword + ",";
                    $scope.status.SetPlugName = "D";
                    $scope.xmlhttp = new XMLHttpRequest();
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            //$scope.ResultChecking = xmlhttp.responseText;
                            if ($scope.xmlhttp.status == 200) {
                                $scope.deviceInformation = $scope.xmlhttp.responseText;
                                arr = $scope.xmlhttp.responseText.split(',');
                                $scope.status.SetPlugName = "E";
                            } else {
                                $scope.status.SetPlugName = "F";
                            }
                            $scope.$apply();
                        }
                    }
                    $scope.xmlhttp.open("GET", endpoint, true);
                    $scope.xmlhttp.send();
                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.SetPlugName = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.SetPlugName = "N";
                    break;
                case "F":
                    $scope.status.SetPlugName = "N";
                    break;
                default:
                    $scope.status.SetPlugName = "N";
                    break;
            }
        }
        $scope.GetInfo = function () {
           
            switch ($scope.status.GetInfo)
            {
                case "N":
                    endpoint = "http://" + $scope.deviceInfo.IP + "/N";
                    $scope.status.GetInfo = "D";
                    $scope.xmlhttp = new XMLHttpRequest();
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            //$scope.ResultChecking = xmlhttp.responseText;
                            if ($scope.xmlhttp.status == 200) {
                                $scope.deviceInformation = $scope.xmlhttp.responseText;
                                arr = $scope.xmlhttp.responseText.split(',');
                                $scope.status.GetInfo = "E";
                            } else {
                                $scope.status.GetInfo = "F";
                            }
                            $scope.$apply();
                        }
                    }
                    $scope.xmlhttp.open("GET", endpoint, true);
                    $scope.xmlhttp.send();
                    
                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.GetInfo = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.GetInfo = "N";
                    break;
                case "F":
                    $scope.status.GetInfo = "N";
                    break;
                default:
                    $scope.status.GetInfo = "N";
                    break;
            }
        }


        $scope.SetServer = function () {
            switch ($scope.status.SetServer) {
                case "N":
                    //http://192.168.1.37/O=192.168.1.36,443,T,
                    arrURL = $scope.deviceInfo.URL.split(":");
                    isCallServer = ($scope.deviceInfo.callServer) ? "T" : "F";
                    endpoint = "http://" + $scope.deviceInfo.IP + "/O=" + arrURL[0] + "," + arrURL[1] + "," + isCallServer+",";
                    $scope.status.SetServer = "D";
                    $scope.xmlhttp = new XMLHttpRequest();
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            //$scope.ResultChecking = xmlhttp.responseText;
                            if ($scope.xmlhttp.status == 200) {
                                $scope.deviceInformation = $scope.xmlhttp.responseText;
                                arr = $scope.xmlhttp.responseText.split(',');
                                $scope.status.SetServer = "E";
                            } else {
                                $scope.status.SetServer = "F";
                            }
                            $scope.$apply();
                        }
                    }
                    $scope.xmlhttp.open("GET", endpoint, true);
                    $scope.xmlhttp.send();

                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.SetServer = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.SetServer = "N";
                    break;
                case "F":
                    $scope.status.SetServer = "N";
                    break;
                default:
                    $scope.status.SetServer = "N";
                    break;
            }
        }

       
        $scope.createCORSRequest  = function(method, url) {
            var xhr = new XMLHttpRequest();
            if ("withCredentials" in xhr) {

                // Check if the XMLHttpRequest object has a "withCredentials" property.
                // "withCredentials" only exists on XMLHTTPRequest2 objects.
                xhr.open(method, url, true);

            } else if (typeof XDomainRequest != "undefined") {

                // Otherwise, check if XDomainRequest.
                // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
                xhr = new XDomainRequest();
                xhr.open(method, url);

            } else {

                // Otherwise, CORS is not supported by the browser.
                xhr = null;

            }
            return xhr;
        }

        

        $scope.CheckingDevice = function()
        {
              
            switch ($scope.status.IPConnected) {
                case "N":
                    endpoint = "http://" + $scope.deviceInfo.IP + "/@A";
                    $scope.status.IPConnected = "D";
                    //$scope.xmlhttp = new XMLHttpRequest();
                    $scope.xmlhttp = $scope.createCORSRequest('GET',endpoint);
                    method = "GET";
                   
                    //
                    $scope.xmlhttp.open(method.toUpperCase(), endpoint, true);
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            //$scope.ResultChecking = xmlhttp.responseText;
                            if ($scope.xmlhttp.status == 200) {
                                str = $scope.xmlhttp.responseText;
                                $scope.status.IPConnected =  ($scope.xmlhttp.responseText.split(',')[1] == "SU") ? "E" : "F";
                            } else {
                               
                                $scope.status.IPConnected = "F";
                            }
                            $scope.$apply();
                        }                       
                    }
                    
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.responseType = "text";
                    //$scope.xmlhttp.setRequestHeader("Accept", "*/*");
                    //$scope.xmlhttp.setRequestHeader("contentType", "text/plain");
                    //$scope.xmlhttp.setRequestHeader("Access-Control-Allow-Origin", "*");
                    //$scope.xmlhttp.setRequestHeader("CSP", "active");
                    //$scope.xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    $scope.xmlhttp.send();
                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.IPConnected = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.IPConnected = "N";
                    break;
                case "F":
                    $scope.status.IPConnected = "N";
                    break;
                default:
                    $scope.status.IPConnected = "N";
                    break;
            }
            /*xmlhttp = new XMLHttpRequest();

            xmlhttp.withCredentials = false;
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $scope.ResultChecking = xmlhttp.responseText;
                }
                $scope.ResultChecking = xmlhttp.status;
            }
            xmlhttp.open("GET", endpoint, true);

            xmlhttp.send();*/
        }

        $scope.RefeshStatusPower = function()
        {

            switch ($scope.status.SendRefreshStatus) {
                case "N":
                    $scope.ResultRefresh = "Connecting...";
                    endpoint = "http://" + $scope.deviceInfo.IP + "/@H";
                    $scope.xmlhttp = new XMLHttpRequest();
                    $scope.status.SendRefreshStatus= "D";
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            if ($scope.xmlhttp.status == 200) {
                                $scope.ResultRefresh = $scope.xmlhttp.responseText;
                                resultPlug = $scope.xmlhttp.responseText.split(','); //C,0000,0000;C,{power},{lock}
                                $scope.deviceInfo.power1 = (resultPlug[1][0] == "1")?true:false;
                                $scope.deviceInfo.power2 = (resultPlug[1][1] == "1") ? true : false;
                                $scope.deviceInfo.power3 = (resultPlug[1][2] == "1") ? true : false;
                                $scope.deviceInfo.power4 = (resultPlug[1][3] == "1") ? true : false;
                                $scope.status.SendRefreshStatus = "E";

                            } else {
                                $scope.status.SendRefreshStatus = "F";
                            }
                            $scope.$apply();
                        }

                        
                    }
                    $scope.xmlhttp.open("GET", endpoint, true);

                    $scope.xmlhttp.send();
                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.SendRefreshStatus = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.SendRefreshStatus = "N";
                    break;
                case "F":
                    $scope.status.SendRefreshStatus = "N";
                    break;
                default:
                    $scope.status.SendRefreshStatus = "N";
                    break;
            }
            
        }

        $scope.ConnectWifi = function () {
           
            switch ($scope.status.WifiAPconnected) {
                case "N":
                    endpoint = "http://" + $scope.deviceInfo.IP + "/A=" + $scope.deviceInfo.APName + "," + $scope.deviceInfo.APpassword+',';
                    $scope.status.WifiAPconnected = "D";
                    $scope.xmlhttp = new XMLHttpRequest();
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            //$scope.ResultChecking = xmlhttp.responseText;
                            if ($scope.xmlhttp.status == 200) {
                                str = $scope.xmlhttp.responseText;
                                var arrString = $scope.xmlhttp.responseText.split(',');
                                $scope.status.WifiAPconnected = "F";
                                for (var i = 0; i < arrString.length; i++) {
                                    if (arrString[i][0] == "O" && arrString[i][1] == "K")
                                    {
                                        $scope.status.WifiAPconnected = "E";
                                    }
                                }
                                //$scope.status.WifiAPconnected = ($scope.xmlhttp.responseText.split(',')[1] == "SU") ? "E" : "F";
                            } else {
                                $scope.status.WifiAPconnected = "F";
                            }
                            $scope.$apply();
                        }
                    }
                    $scope.xmlhttp.open("GET", endpoint, true);
                    $scope.xmlhttp.send();
                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.WifiAPconnected = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.WifiAPconnected = "N";
                    break;
                case "F":
                    $scope.status.WifiAPconnected = "N";
                    break;
                default:
                    $scope.status.WifiAPconnected = "N";
                    break;
            }
           

        }
        $scope.sendPower = function () {
            switch ($scope.status.SendPower) {
                case "N": 
                    power = ($scope.deviceInfo.power1) ? "1" : "0";
                    power+= ($scope.deviceInfo.power2) ? "1" : "0";
                    power+= ($scope.deviceInfo.power3) ? "1" : "0";
                    power+= ($scope.deviceInfo.power4) ? "1" : "0";
                    //http://192.168.4.1//@J1111
                    endpoint = "http://" + $scope.deviceInfo.IP + "/@J" + power;

                    $scope.status.SendPower = "D";
                    $scope.xmlhttp = new XMLHttpRequest();
                    $scope.xmlhttp.withCredentials = false;
                    $scope.xmlhttp.onreadystatechange = function () {
                        if ($scope.xmlhttp.readyState == 4) {
                            //$scope.ResultChecking = xmlhttp.responseText;
                            if ($scope.xmlhttp.status == 200) {
                                $scope.ResultRefresh = $scope.xmlhttp.responseText;

                                $scope.status.SendPower = "E";
                            } else {
                                $scope.status.SendPower = "F";
                            }
                            $scope.$apply();
                        }
                    }
                    $scope.xmlhttp.open("GET", endpoint, true);
                    $scope.xmlhttp.send();
                    break;
                case "D":
                    $scope.xmlhttp.abort();
                    $scope.status.SendPower = "F";
                    $scope.$apply();
                    break;
                case "E":
                    $scope.status.SendPower = "N";
                    break;
                case "F":
                    $scope.status.SendPower = "N";
                    break;
                default:
                    $scope.status.SendPower = "N";
                    break;
            }
        }
        $scope.changeAP = function () {
           
            switch($scope.apWifi)
            {
                case 'Home':
                    $scope.deviceInfo.APName = 'true_homewifi_4CA';
                    $scope.deviceInfo.APpassword= '00000AZH';
                    break;
                case 'Nimit':
                    $scope.deviceInfo.APName = 'VIRUSABLE';
                    $scope.deviceInfo.APpassword = 'azsxdcfv';
                    break;
                case 'Eagle':
                    $scope.deviceInfo.APName = 'true_homewifi_4C7';
                    $scope.deviceInfo.APpassword = '00000H40';
                    break;
            }
            $scope.$apply();
        }
    });
