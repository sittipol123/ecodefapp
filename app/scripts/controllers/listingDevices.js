﻿'use strict';
var app = angular.module('sbAdminApp');

app.controller('devicesCtrl', function ($scope, ENDPOINTHEAD, ENDPOINTTAIL_STATUS, ENDPOINTTAIL_INPUT, $http, $interval) {

   
    $scope.endpointInput = 'http://' + ENDPOINTHEAD + ENDPOINTTAIL_INPUT;
    $scope.lineOption = {
        scaleShowLabels:false
    }

    $scope.line = {
        labels: ['', '', '', '', '', '', ''],
        series: ['Series A'],
        data: [
	      [28, 48, 40, 19, 86, 27, 90]
        ],
        colours: [{
            strokeColor: 'rgba(255, 255, 255, 1)',
            fillColor: 'rgba(255, 255, 255, 0.1)',
        }],
        onClick: function (points, evt) {
          
        }
    };

    $scope.devices = [];
    $scope.deviceIdex = [];
    $scope.xmlhttp = new XMLHttpRequest();
    $scope.xmlhttp.withCredentials = false;
    $scope.xmlhttp.onreadystatechange = function () {
        if ($scope.xmlhttp.readyState == 4) {
     
            if ($scope.xmlhttp.status == 200) {

                var data = JSON.parse($scope.xmlhttp.responseText);
                var keys = Object.keys(data);

                for (var i = 0; i < keys.length; i++) {
                    $scope.deviceIdex[keys[i]] = i;
                    $scope.devices[i] = data[keys[i]];
                    $scope.devices[i].SendPower = "N";
                }
                $scope.IntervalUpdateInput();
            } else {
              
            }
            $scope.$apply();
        }
    }

    $scope.IntervalUpdateInput = function () {
        $interval(function () {
            $scope.UpdateInput();
        }, 5000);
    }
    $scope.sendPower = function(device)
    {
        //device.input['p1'].value
        //device.input['p2'].value
        if(!device.SendPower)
        {
            device.SendPower = "N";
        }
        switch (device.SendPower) {
            case "N": 
                var power = (device.input['p1']) ? device.input['p1'].value : "0";
                power+= (device.input['p2']) ? device.input['p2'].value : "0";
                power+= (device.input['p3']) ? device.input['p3'].value : "0";
                power+= (device.input['p4']) ? device.input['p4'].value : "0";
                //http://192.168.4.1//@J1111
                endpoint = "http://" + device.ip + "/@J" + power;

                device.SendPower = "D";
                device.xmlhttp = new XMLHttpRequest();
                device.xmlhttp.withCredentials = false;
                device.xmlhttp.onreadystatechange = function () {
                    if (device.xmlhttp.readyState == 4) {
                        //$scope.ResultChecking = xmlhttp.responseText;
                        if (device.xmlhttp.status == 200) {
                            device.ResultRefresh = device.xmlhttp.responseText;

                            device.SendPower = "E";
                        } else {
                            device.SendPower = "F";
                        }
                        device.$apply();
                    }
                }
                device.xmlhttp.open("GET", endpoint, true);
                device.xmlhttp.send();
                break;
            case "D":
                device.xmlhttp.abort();
                device.SendPower = "F";
                $scope.$apply();
                break;
            case "E":
                device.SendPower = "N";
                break;
            case "F":
                device.SendPower = "N";
                break;
            default:
                device.SendPower = "N";
                break;
        }
    }
    
    $scope.UpdateInput = function ()
    {
        $http.get($scope.endpointInput).
        success(function (data) {
            
            for (var i = 0; i < data.length; i++) {
                var device = $scope.devices[$scope.deviceIdex[data[i].nodeid]];
                if (device != null) {
                    device.input[data[i].name].value = data[i].value;
                }
            }
            
        });

        $scope.$apply();
    }

    var endpoint = 'http://' + gENDPOINTHEAD + gENDPOINTTAIL_STATUS;
    $scope.xmlhttp.open("GET", endpoint, true);
    $scope.xmlhttp.send();

    
    
});